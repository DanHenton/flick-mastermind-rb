require './lib/game_engine'
require './lib/game_presenter'

secret = 'CDEA'
game = GameEngine.new(secret: secret)
presenter = GamePresenter.new(game)

puts "Welcome to Flick Mastermind 😎

Your adversary has encoded some valuable information using a secret - it's your
job to guess it!

  ????

The secret is four characters long, and consists of any of the following 8
letters:

A B C D E F G H

When you submit a guess, you get a hint back:
- If one of your letters is correct, the hint will contain a X

- If one of your letters is correct but in the wrong position, the hint will
  contain a O

- The position of the X and O in the hint does not necssarily match the position of
  the guessed letter. (i.e. for a secret 'AABC', if your guess was 'CADB' then
  'XO--' would be the hint)

Good luck!

------------------------

Enter your guess:"

until game.finished?
  guess = gets
  game.turn(guess)
  game.finished? ? presenter.success : presenter.continue
end
