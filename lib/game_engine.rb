class GameEngine
  attr_reader :guesses
  SUCCESS_CHECKSUM = 'XXXX'.freeze

  def initialize(secret:)
    @secret = secret
    @guesses_left = 10
    @guesses = []
    @finished = false
  end

  def generate_checksum(guess)
    # TODO: Finish this method!
    '----'
  end

  def turn(guess)
    guess =  guess.strip
    @guesses << guess
    @guesses_left -= 1
    checksum = generate_checksum(guess)
    @finished = true if checksum == SUCCESS_CHECKSUM
  end

  def turns_remaining
    @guesses_left
  end

  def finished?
    @finished
  end
end
