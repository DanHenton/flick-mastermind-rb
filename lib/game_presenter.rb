class GamePresenter
  def initialize(game)
    @game = game
  end

  def continue
    puts "\n\n"
    present_attempts_left
    @game.guesses.each_with_index { |guess, index| present_guess(guess, index) }
    next_guess
  end

  def success
    puts "You guessed the code! 🎉"
  end

  private

  def present_attempts_left
    puts "You have #{@game.turns_remaining} guesses remaining\n\n"
  end

  def present_guess(guess, index)
    puts "GUESS #{index + 1}: #{guess}\t\tCHECKSUM: #{@game.generate_checksum(guess)}"
  end

  def next_guess
    puts "\nPlease enter your next guess:"
  end
end
