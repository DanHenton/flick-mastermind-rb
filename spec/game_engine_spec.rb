require_relative '../lib/game_engine.rb'

describe 'GameEngine' do
  describe '.generate_checksum' do
    describe 'it returns the correct checksum for code ABCD' do
      let(:code) { 'ABCD' }
      subject { GameEngine.new(secret: code) }

      it 'returns OOOO for the guess CADB' do
        expect(subject.generate_checksum('CADB')).to eq 'OOOO'
      end

      it 'returns OOOO for the guess EFGH' do
        expect(subject.generate_checksum('EFGH')).to eq '----'
      end

      it 'returns OOOO for the guess ABEA' do
        expect(subject.generate_checksum('ABEA')).to eq 'XX--'
      end

      it 'returns OOOO for the guess FBDE' do
        expect(subject.generate_checksum('FBDE')).to eq 'XO--'
      end

      it 'returns OOOO for the guess DEFG' do
        expect(subject.generate_checksum('DEFG')).to eq 'O---'
      end

      it 'returns OOOO for the guess DECG' do
        expect(subject.generate_checksum('DECG')).to eq 'XO--'
      end

      it 'returns XXXX for the guess ABCD' do
        expect(subject.generate_checksum('ABCD')).to eq 'XXXX'
      end
    end

    describe 'it returns the correct checksum for code EFED' do
      let(:code) { 'EFED' }
      subject { GameEngine.new(secret: code) }

      it 'returns XX-- for the guess EEEE' do
        expect(subject.generate_checksum('EEEE')).to eq 'XX--'
      end

      it 'returns XO-- for the guess CEEB' do
        expect(subject.generate_checksum('CEEB')).to eq 'XO--'
      end

      it 'returns XXX- for the guess EEED' do
        expect(subject.generate_checksum('EEED')).to eq 'XXX-'
      end

      it 'returns XX-- for the guess EEEE' do
        expect(subject.generate_checksum('EEEE')).to eq 'XX--'
      end

      it 'returns XOO- for the guess DEEE' do
        expect(subject.generate_checksum('DEEE')).to eq 'XOO-'
      end

      it 'returns XOOO for the guess DEEF' do
        expect(subject.generate_checksum('DEEF')).to eq 'XOOO'
      end

      it 'returns ---- for the guess AACG' do
        expect(subject.generate_checksum('AACG')).to eq '----'
      end

      it 'returns XXXX for the guess EFED' do
        expect(subject.generate_checksum('EFED')).to eq 'XXXX'
      end

      it 'returns X0-- for the guess FAEF' do
        expect(subject.generate_checksum('FAEF')).to eq 'XO--'
      end
    end
  end
end
